<?php
/******************************************************************************
* UW Madison Library Resources Block - Version Identifier
*
* Moodle code fragment to establish plugin version and cron cycle.
*
* Author: Nick Koeppen
******************************************************************************/
$plugin->version = 2012100202;
$plugin->requires  = 2014051200;        // Requires this Moodle version
$plugin->component = 'block_wisclibraries';
