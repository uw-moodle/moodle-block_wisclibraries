<?php
/******************************************************************************
* UW Madison Library Resources Block
*
* Generate links to the UW libraries' resources.
*
* Author: Nick Koeppen
******************************************************************************/
/**
 * WISC libraries block system upgrade scripts.
 *
 * @package    blocks
 * @subpackage wisclibraries
 * @copyright  2012 Nick Koeppen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_block_wisclibraries_upgrade($oldversion=0) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    // Upgrade to new version of syncing links with sorting
    if ($oldversion < 2012100100) {
        $table = new xmldb_table('block_wisclibraries');

        // Change field name 'link' to 'url' to avoid confusion of terms
        $field = new xmldb_field('link', XMLDB_TYPE_TEXT, null, null, !XMLDB_NOTNULL, false, null, 'display');
        $dbman->rename_field($table, $field, 'url');

        // Add new sort order field
        $field = new xmldb_field('sortorder', XMLDB_TYPE_INTEGER, '4', null, !XMLDB_NOTNULL, false, null, 'groupid');
        $dbman->add_field($table, $field);

        upgrade_plugin_savepoint(true, 2012100100, 'block', 'wisclibraries');
    }

    return true;
}

?>
