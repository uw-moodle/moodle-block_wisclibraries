<?php
/******************************************************************************
* UW Madison Library Resources Block
*
* Handle events pertaining to WISC libraries data sync.
*
* Author: Nick Koeppen
******************************************************************************/

/**
 * WISC libraries plugin event handler definition.
 *
 * @package    block
 * @subpackage wisclibraries
 * @category   event
 * @copyright  2012 University of Wisconsin-Madison
 * @author     Nick Koeppen
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/* List of handlers */
$handlers = array (

    'course_deleted' => array (
        'handlerfile'      => '/blocks/wisclibraries/lib.php',
        'handlerfunction'  => 'block_wisclibraries_delete_course',
        'schedule'         => 'cron',
        'internal'         => 1,
    ),
    'groups_group_deleted' => array (
        'handlerfile'      => '/blocks/wisclibraries/lib.php',
        'handlerfunction'  => 'block_wisclibraries_delete_group',
        'schedule'         => 'cron',
        'internal'         => 1,
    ),

);