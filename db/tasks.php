<?php
/**
 * Block wisclibraries plugin scheduled tasks.
 *
 * @package    block_wisclibraries
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$tasks =
array(
    array(
            'classname' => 'block_wisclibraries\task\sync',
            'blocking' => 0,
            'minute' => '20',
            'hour' => '1',
            'day' => '*',
            'dayofweek' => '*',
            'month' => '*'
    ),
);