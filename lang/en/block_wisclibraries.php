<?php
$string['pluginname'] = 'WISC Library Resources';
$string['block_title'] = 'Library Resources';

$string['wisclibraries:manage'] = 'Manage LCP links';
$string['wisclibraries:viewall'] = 'View all LCP links';

$string['displaygroupname'] = 'Group Name Display';
$string['displaygroupnamedesc'] = 'Whether the display should be the Moodle group name instead of the default from the library.';
$string['lcpprefix'] = 'LCP Prefix';
$string['lcpprefixdesc'] = 'The root site address for Library Course Page (LCP) XML feeds.';
$string['lcpprefixdefault'] = 'https://lcp.library.wisc.edu/courses/campus_course_id/';
$string['linkdepth'] = 'Link Depth';
$string['linkdepthdesc'] = 'The extent to which the block should display links from LCP information.  Note: Tabs refer to the tabs within each LCP page.';
$string['page'] = 'LCP Main Page';
$string['refresh'] = 'Refresh links';
$string['synctask'] = 'Synchronize WISC library resources';
$string['tabs'] = 'LCP Tabs';

