<?php
/**
 * Sync wisc library content task.
 *
 * @copyright 2015 University of Wisconsin
 * @author Matt Petro
 */

namespace block_wisclibraries\task;

require_once($CFG->dirroot."/blocks/wisclibraries/lib.php");

defined('MOODLE_INTERNAL') || die();

class sync extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens
        return get_string('synctask', 'block_wisclibraries');
    }

    public function execute() {
        block_wisclibraries_sync();
    }
}