****************************************************************************************************
UW-Madison Moodle plugins
- AUTHOR: Nick Koeppen (nkoeppen@wisc.edu)
- UPDATE: 21 Feb 2012
****************************************************************************************************
- TYPE: block
- NAME: wisclibraries (WISC Libraries)
****************************************************************************************************
*****  Description   *****
This block provides links to the UW Libraries Course Pages (LCPs).  These links are supplied by the
UW Libraries XML feeds that are also used to build the page itself.

***** Default blocks *****
Moodle 2.x:

Default blocks in Moodle 2.x are hard coded in config.php by setting the defaultblocks_override
variable.  Refer to config-dist.php for more documentation.

- Format:
* The block names are in CSV (comma-separated values).
* The colon separates blocks on the left and right sides.

- Example:

$CFG->defaultblocks_override = 'participants,course_list:wisclibraries,news_items';