<?php

function block_wisclibraries_delete_course($course){
    global $DB;

    $DB->delete_records('block_wisclibraries', array('courseid' => $course->id));
}

function block_wisclibraries_delete_group($group){
    global $DB;

    $DB->delete_records('block_wisclibraries', array('id' => $group->id));
}

function block_wisclibraries_page_records($criteria, $xmlcourse, $xmlsection){
    global $DB;

    $config = get_config("block_wisclibraries");

    $link = new stdClass();
    $link->url = $xmlcourse->url;
    $link->name = $xmlsection->component.' '.$xmlsection->section;

    $record = $DB->get_record("block_wisclibraries", $criteria);

    block_wisclibraries_update_record($record, $criteria, $link);
}

function block_wisclibraries_sync($courseid = null, $onlycurrent = true){
    global $CFG, $DB;

    $config = get_config('block_wisclibraries');

    /* Store current terms (and test CHUB service) to check archives quickly */
    if ($onlycurrent) {
        $now = time();
        $currentTerms = array();
        $ds = new \enrol_wisc\local\chub\chub_datasource();
        $wiscTerms = $ds->getAvailableTerms();

        // Set current terms from CHUB service values
        foreach($wiscTerms as $wiscTerm){
            if ($wiscTerm->endDate > $now) {
                $currentTerms[] = $wiscTerm->termCode;
            }
        }
    }

    /* Retrieve subset of enrol wisc sections to sync */
    $params = array();
    $conditions = array();
    if ($courseid) {
        $conditions[] = "courseid = :courseid";
        $params['courseid'] = $courseid;
    }
    if ($onlycurrent) {
        list($tsql, $tparams) = $DB->get_in_or_equal($currentTerms, SQL_PARAMS_NAMED);
        $conditions[] = "term $tsql";
        $params += $tparams;
    }
    $select = implode(' AND ', $conditions);
    $sort = 'isis_course_id, term, subject_code, catalog_number, section_number';
    $sections = $DB->get_records_select("enrol_wisc_coursemap", $select, $params, $sort);

    /* Match LCP resources with local records */
    $xmldata = array();
    $to_delete = array();
    foreach($sections as $section){
        $criteria = array('courseid' => $section->courseid, 'groupid' => $section->groupid);

        // Get course LCP XML data
        $xmlurl = $config->lcpprefix.$section->isis_course_id.'?term_code='.$section->term;
        if (!array_key_exists($xmlurl, $xmldata)) {
            $xmldata[$xmlurl] = simplexml_load_file($xmlurl);
        }
        $lcpxml = $xmldata[$xmlurl];
        if (!$lcpxml->course_pages) {
            continue;		// Skip pages without LCP information
        }

           foreach($lcpxml->course_pages->children() as $course){
            foreach($course->sections->children() as $lcpsection){
                if ($lcpsection->course_number != $section->catalog_number ||
                        $lcpsection->section != (int)$section->section_number) {
                    continue;
                }

                // Section match - same class, catalog, and section number
                switch($config->linkdepth){
                    default:
                    case 'tabs':
                        $oldrecs = block_wisclibraries_tab_records($criteria, $course);
                        $to_delete = array_merge($to_delete, array_keys($oldrecs));
                        break;
                    case 'page':
                        block_wisclibraries_page_records($criteria, $course, $lcpsection);
                        break;
                }
            }
        }
    }

    // Delete no longer valid links
    if (!empty($to_delete)) {
        $DB->delete_records_list('block_wisclibraries', 'id', $to_delete);
    }

    return true;
}

function block_wisclibraries_tab_records($criteria, $xmlcourse){
    global $DB;

    $oldrecords = $DB->get_records("block_wisclibraries", $criteria);

    /* Check each tab in this subtree for links to display */
    foreach ($xmlcourse->tabs as $tab) {
        $order = 1;
        foreach($tab as $link){
            /* Sync database records with current XML */
            $entry = null;
            foreach($oldrecords as $key => $record){
                if ($record->display == $link->name || $record->url == $link->url) {
                    // Name or URL matched - allows either to change
                    $entry = $record;
                    unset($oldrecords[$key]); //Updating, not deleting
                }
            }

            block_wisclibraries_update_record($entry, $criteria, $link, $order);

            $order++;
        }
    }

    return $oldrecords;
}

function block_wisclibraries_update_record($record, $criteria, $link, $order = null){
    global $DB;

    if ($record) {
        if ($record->url != (string)$link->url || $record->sortorder != $order ||
                $record->display != (string)$link->name) {
            $record->url = (string)$link->url;
            $record->display = (string)$link->name;
            $record->sortorder = $order;
            $record->timemodified = time();
            $DB->update_record("block_wisclibraries", $record);
        }
    } else {
        $record = new stdClass();
        $record->courseid = $criteria['courseid'];
        $record->groupid = $criteria['groupid'];
        $record->url = (string)$link->url;
        $record->display = (string)$link->name;
        $record->sortorder = $order;
        $record->timemodified = time();
        $DB->insert_record("block_wisclibraries", $record);
    }
}
