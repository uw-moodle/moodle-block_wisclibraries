<?php
/******************************************************************************
* UW Madison Library Resources Block
*
* Generate links to the UW libraries' resources.
*
* Author: Nick Koeppen
******************************************************************************/
require_once($CFG->dirroot."/blocks/wisclibraries/lib.php");

class block_wisclibraries extends block_base{

    protected $_course;

    /**
     * Tell Moodle that we have global configuration settings
     *
     * @return bool
     */
    public function has_config(){
        return true;
    }

    function init() {
        $this->title = get_string('block_title', 'block_wisclibraries');
    }

    function applicable_formats() {
        // Default case: the block can be used in all course types
        return array('site' => false, 'my' => false, 'course-view' => true);
    }

    function get_content(){
        global $DB, $USER, $OUTPUT;

        if ($this->content !== NULL) {
            return $this->content;
        }
        $this->content = new stdClass();

        // Make sure we're in a course context
        if ($this->page->context->get_course_context(false) === false) {
            return $this->content;
        }

        $courseid = $this->get_course()->id;
        if ($courseid == SITEID) {
            return $this->content;
        }

        $html = '';
        if (has_capability('block/wisclibraries:manage', $this->context)) {
            global $PAGE;

            $url = $PAGE->url;
            $url->param('refreshlcplinks', true);
            $icon = new pix_icon('t/reload', get_string('refresh', 'block_wisclibraries'));
            $refresh = $OUTPUT->action_icon($url, $icon, null, null, true);
            $html .= $OUTPUT->container(html_writer::tag('p', $refresh, array('class' => 'centerpara')));
        }

        $viewall = has_capability('block/wisclibraries:viewall', $this->context);

        /* Determine subset of LCP links to show */
        // Get relevant groups
        if ($viewall) {
            $groups = groups_get_all_groups($courseid);              //Get all groups
        } else {
            $groups = groups_get_all_groups($courseid, $USER->id);   //Get user groups
        }

        // Get LCP records sorted by enrolment data
        $joincond = 'e.courseid = l.courseid';
        if (!empty($groups)) {
            $joincond .= ' AND e.groupid = l.groupid';
            list($gsql, $params) = $DB->get_in_or_equal(array_keys($groups), SQL_PARAMS_NAMED);
            $gsql = "l.groupid IS NULL OR l.groupid $gsql";
        } else {
            $joincond .= ' AND e.groupid IS NULL';
            $gsql = 'l.groupid IS NULL';
            $params = array();
        }
        $tables = "{block_wisclibraries} l LEFT JOIN {enrol_wisc_coursemap} e ON $joincond";
        $where = "l.courseid = :course AND ($gsql)";
        $params['course'] = $courseid;
        $sort = 'e.subject_code, e.catalog_number, e.section_number, l.sortorder';
        $sql = "SELECT l.* FROM $tables WHERE $where ORDER BY $sort";
        $records = $DB->get_records_sql($sql, $params);

        if (!empty($records)) {
            $config = get_config('block_wisclibraries');
            $pagelinks = ($config->linkdepth == 'page');
            $showgroupnames = !$pagelinks && ($viewall || count($groups) > 1);

            /* Collect URLs to create links grouped by group */
            $data = array();
            foreach($records as $record){
                if ($pagelinks && $config->displaygroupname && $record->groupid && !empty($groups)) {
                    $text = $groups[$record->groupid]->name;
                } else {
                    $text = $record->display;
                }
                $data[$record->groupid][$record->url] = $text;
            }

            /* Group all common list of links - string compare not possible with action link ids */
            $currdata = array();
            $lists = array();
            $listattr = array('class' => $config->linkdepth.'link');
            foreach($data as $groupid => $grpdata){
                if ($grpdata !== $currdata) {
                    $links = array();
                    // Only create the list when different
                    foreach($grpdata as $url => $text) {
                        $action = new popup_action('click', $url, 'wisclibpopup', array('fullscreen' => true));
                        $links[] = $OUTPUT->action_link($url, $text, $action);
                    }
                    $newlist = new stdClass();
                    $newlist->groups = array();
                    $newlist->links  = html_writer::alist($links, $listattr);
                    $lists[] = $newlist;

                    $currdata = $grpdata;
                }
                $list = end($lists);
                $list->groups[] = $groupid;
            }

            /* Display list of links below all applicable groups */
            foreach($lists as $list) {
                if ($showgroupnames) {
                    foreach($list->groups as $groupid){
                        // Display group name
                        $group = $groups[$groupid];
                        $html .= html_writer::tag('p', $group->name, array('class' => 'groupname'));
                    }
                }
                $html .= $list->links;
            }
        }

        $this->content->text = $html;
        return $this->content;
    }

    function get_course(){
        if (!isset($this->_course)) {
            global $DB;

            $ccontext = $this->context->get_course_context();
            $this->_course = $DB->get_record('course', array('id' => $ccontext->instanceid));
        }

        return $this->_course;
    }

    function specialization() {
        global $PAGE;

        if (optional_param('refreshlcplinks', false, PARAM_BOOL)) {
            // Very hacky workaround when debugging causes messages to
            // start printing header before redirect
            if ($PAGE->pagelayout == 'redirect') {
                return;
            }
            if (has_capability('block/wisclibraries:manage', $this->context)) {
                // Sync links now
                block_wisclibraries_sync($this->get_course()->id, false);
            }

            $url = $PAGE->url;
            $url->remove_params('refreshlcplinks');
            redirect($url);
        }
    }

}
?>
