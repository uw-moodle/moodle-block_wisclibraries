<?php
/******************************************************************************
* UW Madison Library Resources Block - Global Settings
*
* Global (site-wide) configuration settings for Library Resources block.
*
* Author: Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $blockname = 'block_wisclibraries';
    $curr_config = get_config($blockname);

    if (isset($curr_config->linkdepth)) {
        /* Detect change to link setting to make DB purges */
        if (!isset($curr_config->dblinks) || $curr_config->dblinks != $curr_config->linkdepth) {
            global $DB;

            // Remove current records
            $DB->delete_records('block_wisclibraries');
            // Allow cron update on next cycle
            $DB->set_field('block', 'lastcron', 0, array('name' => 'wisclibraries'));
        }
        set_config('dblinks', $curr_config->linkdepth, $blockname);
    }

    $configs = array();
    $name = 'lcpprefix';
    $visname = get_string('lcpprefix', $blockname);
    $descrip = get_string('lcpprefixdesc', $blockname);
    $default = get_string('lcpprefixdefault', $blockname);
    $configs[] = new admin_setting_configtext($name, $visname, $descrip, $default, PARAM_URL);

    $name = 'linkdepth';
    $visname = get_string('linkdepth', $blockname);
    $descrip = get_string('linkdepthdesc', $blockname);
    $default = 'tabs';
    $depths = array(
        'page' => get_string('page', $blockname),
        'tabs' => get_string('tabs', $blockname),
    );
    $configs[] = new admin_setting_configselect('linkdepth', $visname, $descrip, $default, $depths);

    if (isset($curr_config->linkdepth) && $curr_config->linkdepth == 'page') {
        $name = 'displaygroupname';
        $visname = get_string('displaygroupname', $blockname);
        $descrip = get_string('displaygroupnamedesc', $blockname);
        $default = false;
        $configs[] = new admin_setting_configcheckbox($name, $visname, $descrip, $default);
    }

    foreach ($configs as $config) {
        $config->plugin = $blockname;
        $settings->add($config);
    }
}